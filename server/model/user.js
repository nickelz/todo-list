const mongoose = require('mongoose')
const validator = require('validator')
const jwt = require('jsonwebtoken')
const _ = require('lodash')
const bcrypt = require('bcryptjs')

var UserSchema = new mongoose.Schema({
	email: {
		type: String,
		required: true,
		trim: true,
		minlength: 1,
		unique: true,
		validate: {
			validator: validator.isEmail,
			message: '{VALUE} is not a valid email'
		}
	},
	
	password: {
		type: String,
		required: true,
		minlength: 6
	},
	
	tokens: [{
		access: {
			type: String,
			required: true
		},
		
		token: {
			type: String,
			required: true
		}
	}]
})

// Instance Method ( Things you do with the instance itself )
// Overriding the pre-existing toJSON method
UserSchema.methods.toJSON = function () {
	var user = this
	var userObject = user.toObject()
	
	return _.pick(userObject, ['_id', 'email'])
}

// Creating a NEW method
UserSchema.methods.generateAuthToken = function () {
	var user = this
	var access = 'auth'
	var token = jwt.sign({ _id: user._id.toHexString(), access }, process.env.JWT_SECRET).toString()
	user.tokens.push({ access, token })
	
	return user.save().then(() => {
		return token
	})
}

UserSchema.methods.removeToken = function (token) {
	var user = this
	
	return user.update({
		$pull: { // The $pull operator removes one or more instance(s) that match a specific condition
			tokens: { token }
		}
	})
}

// Model Method ( Things you do to the whole model )
UserSchema.statics.findByToken = function (token) {
	var User = this
	var decoded
	
	try {
		decoded = jwt.verify(token, process.env.JWT_SECRET)
	} catch (e) {
		return Promise.reject()
	}
	
	return User.findOne({
		'_id': decoded._id,
		'tokens.token': token, // Using quotes to be able to access the .token and .access properties
		'tokens.access': 'auth'
	})
}

UserSchema.statics.findByCredentials = function (email, password) {
	var User = this
	
	return User.findOne({ email }).then((user) => {
		if (!user) {
			return Promise.reject()
		}
		
		return new Promise((resolve, reject) => {
			bcrypt.compare(password, user.password, (err, res) => {
				if (res) {
					resolve(user)
				} else {
					reject()
				}
			})
		})
	})
}

// This method will run before (pre) saving the user into the database
UserSchema.pre('save', function (next) {
	var user = this
	
	// Hashing the password
	if (user.isModified('password')) {
		bcrypt.genSalt(10, (err, salt) => {
			bcrypt.hash(user.password, salt, (err, res) => {
				user.password = res
				next()
			})
		})
	} else {
		next()
	}
})

module.exports = mongoose.model('User', UserSchema)