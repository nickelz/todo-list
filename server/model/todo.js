const mongoose = require('mongoose')

// Variables
var Schmea = mongoose.Schema

var todoSchema = new Schmea({
	text: {
		type: String,
		required: 'Please fill the name of the task',
		minlength: 1,
		trim: true,
	},

	status: {
		type: String,
		enum: ['Pending', 'Ongoing', 'Completed'],
		default: ['Pending']
	},

	date_of_creation: {
		type: Date,
		default: Date.now
	},

	_creator: {
		type: mongoose.Schema.Types.ObjectId,
		required: true
	}
})

module.exports = mongoose.model('Todo', todoSchema)