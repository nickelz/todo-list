require('./config/config')

const express = require('express')
const bodyParser = require('body-parser')
const hbs = require('hbs')

var {
	list_all_todos,
	create_new_todo,
	read_one_todo,
	update_one_todo,
	delete_one_todo
} = require('./controller/todoC')
var {
	create_new_user,
	authenticate_user,
	user_login,
	user_logout
} = require('./controller/userC')
var { authenticate } = require('./middleware/authenticate')

// Varibales
var app = express()
var port = process.env.PORT

// Configuration
app.set('view engine', 'hbs')
app.use(express.static(__dirname + '/public'))
app.use(bodyParser.urlencoded({
	extended: true
}))
app.use(bodyParser.json())

// Routes
app.get('/', (req, res) => res.redirect('/todos'))

app.route('/todos')
	.get(authenticate, list_all_todos)
	.post(authenticate, create_new_todo)

app.route('/todos/:taskId')
	.get(authenticate, read_one_todo)
	.patch(authenticate, update_one_todo)
	.delete(authenticate, delete_one_todo)

app.route('/users')
	.post(create_new_user)

app.route('/users/me')
	.get(authenticate, authenticate_user)

app.route('/users/login')
	.post(user_login)

app.route('/users/me/token')
	.delete(authenticate, user_logout)

app.listen(port, () => console.log(`Server running on port ${port} ..`))