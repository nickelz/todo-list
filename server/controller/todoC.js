var {
	ObjectID
} = require('mongodb')

var mongoose = require('./../db/mongoose')
var Todo = require('./../model/todo')

// GET /todos
var list_all_todos = (req, res) => {
	Todo.find({
		_creator: req.user._id
	}).then((todos) => {
		res.send(todos)
		// res.render('index.hbs', {
		// 	todos
		// })
	}).catch((e) => {
		// res.status(400).send(e)
		return showErrorPage(res, 'Unknown error', e)
	})
}

// POST /todos
var create_new_todo = (req, res) => {
	var todo = new Todo({
		text: req.body.text,
		_creator: req.user._id
	})

	todo.save().then((todo) => {
		res.send(todo)
		console.log(JSON.stringify(todo, undefined, 2))
		// res.redirect('/todos')
	}).catch((e) => {
		// res.status(400).send(e)
		return showErrorPage(res, 'Unknown error', e)
	})
}

// GET /todos/:taskId
var read_one_todo = (req, res) => {
	Todo.findOne({
		_id: req.params.taskId,
		_creator: req.user._id
	}).then((todo) => {
		// res.send(todo)
		if (!todo) {
			return showErrorPage(res, 'Todo don\'t exist', '')
		} else if (!ObjectID.isValid(req.params.taskId)) {
			return showErrorPage(res, 'Invalid ID', '')
		}

		res.render('todo.hbs', {
			todo
		})
	}).catch((e) => {
		// res.status(400).send(e)
		return showErrorPage(res, 'Unknown error', e)
	})
}

// DELETE /todos/:taskId
var delete_one_todo = (req, res) => {
	Todo.findOneAndRemove({
		_id: req.params.taskId,
		_creator: req.user._id
	}).then((todo) => {
		if (!todo) {
			return showErrorPage(res, 'Todo don\'t exist', '')
		} else if (!ObjectID.isValid(req.params.taskId)) {
			return showErrorPage(res, 'Invalid ID', '')
		}

		res.send(todo)
	}).catch((e) => {
		// res.status(400).send(e)
		return showErrorPage(res, 'Unknown error', e)
	})
}

// POST /todos/:taskId
var update_one_todo = (req, res) => {
	Todo.findOneAndUpdate({
		_id: req.params.taskId,
		_creator: req.user._id
	}, req.body, {
		new: true
	}).then((todo) => {
		if (!todo) {
			return showErrorPage(res, 'Todo don\'t exist', '')
		} else if (!ObjectID.isValid(req.params.taskId)) {
			return showErrorPage(res, 'Invalid ID', '')
		}
		res.send(todo)
	}).catch((e) => {
		// res.status(400).send(e)
		return showErrorPage(res, 'Unknown error', e)
	})
}

// Utils
function showErrorPage(res, title, error) {
	res.render('error.hbs', {
		title,
		error
	})
}

// Exports
module.exports = {
	list_all_todos,
	create_new_todo,
	read_one_todo,
	delete_one_todo,
	update_one_todo
}